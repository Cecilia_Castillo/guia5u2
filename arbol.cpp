#include <iostream>
#include <string>
#include <cstring>
using namespace std;
#include "arbol.h"
#define TRUE 1
#define FALSE 0

Arbol::Arbol() {}

void Arbol::InsercionBalanceado(NODO **nodocabeza, int *BO, int infor) {
	NODO *nodo = NULL;
	NODO *nodo1 = NULL;
	NODO *nodo2 = NULL; 
  
	nodo = *nodocabeza;
  
	if (nodo != NULL) {
    
	if (infor < nodo->info) {
      InsercionBalanceado(&(nodo->izq), BO, infor);
      
      if(*BO == TRUE) {
        
        switch (nodo->FE) {
          case 1: 
            nodo->FE = 0;
            *BO = FALSE;
            break;
          
          case 0: 
            nodo->FE = -1;
            break;
     
          case -1: 
            /* reestructuración del árbol */
            nodo1 = nodo->izq;
            
            /* Rotacion II */
            if (nodo1->FE <= 0) { 
              nodo->izq = nodo1->der;
              nodo1->der = nodo;
              nodo->FE = 0;
              nodo = nodo1;
            
            } else { 
              /* Rotacion ID */
              nodo2 = nodo1->der;
              nodo->izq = nodo2->der;
              nodo2->der = nodo;
              nodo1->der = nodo2->izq;
              nodo2->izq = nodo1;
         
              if (nodo2->FE == -1)
                nodo->FE = 1;
              else
                nodo->FE =0;
        
              if (nodo2->FE == 1)
                nodo1->FE = -1;
              else
                nodo1->FE = 0;
              
              nodo = nodo2;
            }
            
            nodo->FE = 0;
            *BO = FALSE;
            break;
        }
      } 
      
    } else {
      
      if (infor > nodo->info) {
        InsercionBalanceado(&(nodo->der), BO, infor);
        
        if (*BO == TRUE) {
          
          switch (nodo->FE) {
            
            case -1: 
              nodo->FE = 0;
              *BO = FALSE;
              break;
            
            case 0: 
              nodo->FE = 1;
              break;
      
            case 1: 
              /* reestructuración del árbol */
              nodo1 = nodo->der;
              
              if (nodo1->FE >= 0) { 
                /* Rotacion DD */
                nodo->der = nodo1->izq;
                nodo1->izq = nodo;
                nodo->FE = 0;
                nodo = nodo1;
                
              } else { 
                /* Rotacion DI */
                nodo2 = nodo1->izq;
                nodo->der = nodo2->izq;
                nodo2->izq = nodo;
                nodo1->izq = nodo2->der;
                nodo2->der = nodo1;
                
                if (nodo2->FE == 1)
                  nodo->FE = -1;
                else
                  nodo->FE = 0;
       
                if (nodo2->FE == -1)
                  nodo1->FE = 1;
                else
                  nodo1->FE = 0;
   
                nodo = nodo2;
              }
              
              nodo->FE = 0;
              BO = FALSE;
              break;
          }
        }
      } else {
        printf("El nodo ya se encuentra en el árbol\n");
      }
    }
  } else {
    
    nodo = (struct _NODO*) malloc (sizeof(NODO));
    nodo->izq = NULL;
    nodo->der = NULL;
    nodo->info = infor;
    nodo->FE = 0;
    *BO = TRUE;
  }
  
  *nodocabeza = nodo;
}

/* */
void Busqueda(NODO *nodo,int infor) {
  if (nodo != NULL) {
    if (infor < nodo->info) {
      Busqueda(nodo->izq,infor);
    } else {
      if (infor > nodo->info) {
        Busqueda(nodo->der,infor);
      } else {
        cout<<("El nodo SI se encuentra en el árbol\n");
      }
    }
  } else {
    cout<<("El nodo NO se encuentra en el árbol\n");
  }
}

/* */
void Arbol::Restructura1(NODO **nodocabeza, int *BO) {
  NODO *nodo, *nodo1, *nodo2; 
  nodo = *nodocabeza;
  
  if (*BO == TRUE) {
    switch (nodo->FE) {
      case -1: 
        nodo->FE = 0;
        break;
      
      case 0: 
        nodo->FE = 1;
        *BO = FALSE;
        break;
   
    case 1: 
      /* reestructuracion del árbol */
      nodo1 = nodo->der;
      
      if (nodo1->FE >= 0) { 
        /* rotacion DD */
        nodo->der = nodo1->izq;
        nodo1->izq = nodo;
        
        switch (nodo1->FE) {
          case 0: 
            nodo->FE = 1;
            nodo1->FE = -1;
            *BO = FALSE;
            break;
          case 1: 
            nodo->FE = 0;
            nodo1->FE = 0;
            *BO = FALSE;
            break;           
        }
        nodo = nodo1;
      } else { 
        /* Rotacion DI */
        nodo2 = nodo1->izq;
        nodo->der = nodo2->izq;
        nodo2->izq = nodo;
        nodo1->izq = nodo2->der;
        nodo2->der = nodo1;
       
        if (nodo2->FE == 1)
          nodo->FE = -1;
        else
          nodo->FE = 0;
        
        if (nodo2->FE == -1)
          nodo1->FE = 1;
        else
          nodo1->FE = 0;
       
        nodo = nodo2;
        nodo2->FE = 0;       
      } 
      break;   
    }
  }
  *nodocabeza=nodo;
}

/* */
void Arbol::Restructura2(NODO **nodocabeza, int *BO) {
  NODO *nodo, *nodo1, *nodo2; 
  nodo = *nodocabeza;
  
  if (*BO == TRUE) {
    switch (nodo->FE) {
      case 1: 
        nodo->FE = 0;
        break;
      case 0: 
        nodo->FE = -1;
        *BO = FALSE;
        break;
      case -1: 
        /* reestructuracion del árbol */
        nodo1 = nodo->izq;
        if (nodo1->FE<=0) { 
          /* rotacion II */
          nodo->izq = nodo1->der;
          nodo1->der = nodo;
          switch (nodo1->FE) {
            case 0: 
              nodo->FE = -1;
              nodo1->FE = 1;
              *BO = FALSE;
              break;
            case -1: 
              nodo->FE = 0;
              nodo1->FE = 0;
              *BO = FALSE;
              break;
          }
          nodo = nodo1;
        } else { 
          /* Rotacion ID */
          nodo2 = nodo1->der;
          nodo->izq = nodo2->der;
          nodo2->der = nodo;
          nodo1->der = nodo2->izq;
          nodo2->izq = nodo1;
       
          if (nodo2->FE == -1)
            nodo->FE = 1;
          else
            nodo->FE = 0;
        
          if (nodo2->FE == 1)
            nodo1->FE = -1;
          else
            nodo1->FE = 0;
       
          nodo = nodo2;
          nodo2->FE = 0;       
        }      
        break;   
    }
  }
  *nodocabeza = nodo;
}

/* */
void Arbol::Borra(NODO **aux1, NODO **otro1, int *BO) {
  NODO *aux, *otro; 
  aux=*aux1;
  otro=*otro1;
  
  if (aux->der != NULL) {
    Borra(&(aux->der),&otro,BO); 
    Restructura2(&aux,BO);
  } else {
    otro->info = aux->info;
    aux = aux->izq;
    *BO = TRUE;
  }
  *aux1=aux;
  *otro1=otro;
}

/* */
void Arbol::EliminacionBalanceado(NODO **nodocabeza, int *BO, int infor) {
  NODO *nodo, *otro; 
  
  nodo = *nodocabeza;
  
  if (nodo != NULL) {
    if (infor < nodo->info) {
      EliminacionBalanceado(&(nodo->izq),BO,infor);
      Restructura1(&nodo,BO);
    } else {
      if (infor > nodo->info) {
        EliminacionBalanceado(&(nodo->der),BO,infor);
        Restructura2(&nodo,BO); 
      } else {
        otro = nodo;
        if (otro->der == NULL) {
          nodo = otro->izq;
          *BO = TRUE;     
        } else {
          if (otro->izq==NULL) {
            nodo=otro->der;
            *BO=TRUE;     
          } else {
            Borra(&(otro->izq),&otro,BO);
            Restructura1(&nodo,BO);
            free(otro);
          }
        }
      }
    } 
  } else {
    cout<<("El nodo NO se encuentra en el árbol\n");
  }
  *nodocabeza=nodo;
}

/* */
void Arbol::GenerarGrafo(NODO *ArbolInt) {
  FILE *fp;
  
  fp = fopen("grafo.txt", "w");
  fprintf(fp, "digraph G {");
  fprintf(fp, " node [style=filled fillcolor=yellow];");
  
  fprintf(fp, "nullraiz [shape=point];\n"); 
  fprintf(fp, [label=int],"nullraiz->", ArbolInt->info, ArbolInt->FE); 
  PreOrden(ArbolInt, fp);
  
  fprintf(fp, "%s\n", "}");
  fclose(fp);
  
  system("dot -Tpng -ografo.png grafo.txt");
  system("eog grafo.png &");
}

/* */
void Arbol::PreOrden(NODO *a, FILE *fp) {
  char cadena[16];
  
  if (a != NULL) {
    if (a->izq != NULL) {
      fprintf(fp, [label=int],a->info,"->",a->izq->info,a->izq->FE); 
    } else{
      sprintf(cadena ,int ,a->info);
      strcat(cadena,"i");
      fprintf(fp, "\"%s\"%s\n", cadena," [shape=point];");
      fprintf(fp, "%d%s\"%s\"%s\n",a->info,"->",cadena,";"); 
    }
    
    if (a->der != NULL) {
      fprintf(fp, "%d%s%d [label=%d];\n",a->info,"->",a->der->info,a->der->FE); 
    } else{
      sprintf(cadena,"%d",a->info);
      strcat(cadena,"d");
      fprintf(fp, "\"%s\"%s\n", cadena," [shape=point];");
      fprintf(fp, int,a->info,"->",cadena,";"); 
    }

    PreOrden(a->izq,fp);
    PreOrden(a->der,fp); 
  }
}
