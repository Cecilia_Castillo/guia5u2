//#include <stdlib>
#include <iostream>
#include <string>
#include "arbol.h"

#define TRUE 1
#define FALSE 0
/* 
 * Muestra un menu de opciones.
 */
int Menu() {
  int Op;
  
  do {
    printf("\n--------------------\n");
    printf("1) Insertar\n");
    printf("2) Buscar\n");
    printf("3) Eliminacion\n");
    printf("4) Grafo\n");
    printf("0) Salir\n\n");
    printf("Opción: ");
    scanf("%d*c", &Op);
  } while (Op<0 || Op>7);
  
  return (Op);
}
/*
 * Función principal
 */
int main(int argc, char **argv) {
  int opcion;
  int elemento;
  NODO *raiz = NULL;
  
  system("clear");
  opcion = Menu();
  int inicio;
  
  while (opcion) {
    
    switch(opcion) {
      case 1:
        cout<<("Ingresar elemento: ");
        cin >> ( &elemento);
        inicio=FALSE;
        InsercionBalanceado(&raiz, &inicio, elemento);
        GenerarGrafo(raiz);
        break;

      case 2:
        cout<<("Buscar elemento: ");
        cin>>(&elemento);
        Busqueda(raiz, elemento);
        break;
      
      case 3:
        cout<<("Eliminar elemento: ");
        cin>>( &elemento);
        inicio=FALSE;
        EliminacionBalanceado(&raiz, &inicio, elemento);
        GenerarGrafo(raiz);
        break;
      
      case 4:
        GenerarGrafo(raiz);
        break;
      
      case 0: 
        exit(0);
    }
    
    opcion = Menu();
  }
  
  return 0;
}

