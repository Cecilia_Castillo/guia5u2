prefix=/usr/local
CC = gcc

CFLAGS = -g -Wall 
SRC = guia5u2.cpp arbol.cpp
OBJ = guia5u2.cpp arbol.cpp
APP = programa

all: $(OBJ)
	$(CC) $(CFLAGS)-o $(APP) $(OBJ) 

clean:
	$(RM) $(OBJ) $(APP)

install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin

uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)

.PHONY: install	
