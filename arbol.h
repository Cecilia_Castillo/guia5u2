#ifndef ARBOL_H
#define ARBOL_H
#include <fstream>
#include <iostream>
#include <string>
using namespace std;

typedef struct _NODO {
  struct _NODO *izq;
  struct _NODO *der;
  string info;
  int FE;
} NODO;

_NODO *arbol = NULL;
/* 
 * crea un nuevo nodo.
 */
_NODO *crearNodo(int dato) {
    _NODO *q;

    q = new _NODO();
    q->izq = NULL;
    q->der = NULL;
    q->info = dato;
    return q;
}

class Arbol {
	private:
		_NODO *raiz = NULL;
		_NODO *ultimo = NULL;

	public:
		/* constructor*/
		Arbol();
        
		/* inserta un nuevo nodo*/
	void InsercionBalanceado(NODO **nodocabeza, int *BO, int infor);
	void Busqueda(NODO *nodo, int infor);
	void Restructura1(NODO **nodocabeza, int *BO);
	void Restructura2(NODO **nodocabeza, int *BO);
	void Borra(NODO **aux1, NODO **otro1, int *BO);
	void EliminacionBalanceado(NODO **nodocabeza, int *BO, int infor);
	int Menu();
	void GenerarGrafo(NODO *p);
	void PreOrden(NODO *, FILE *fp);


};
#endif
